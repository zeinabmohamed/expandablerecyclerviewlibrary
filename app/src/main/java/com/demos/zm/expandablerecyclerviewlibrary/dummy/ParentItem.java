package com.demos.zm.expandablerecyclerviewlibrary.dummy;

import java.util.List;

/**
 * Created by Zeinab Mohamed on 5/29/2016.
 */
public class ParentItem {

    private int id;
    private String title;

    List<DummyContent.DummyItem> dummyItems;

    public ParentItem(int id, String title, List<DummyContent.DummyItem> dummyItems) {
        this.id = id;
        this.title = title;
        this.dummyItems = dummyItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<DummyContent.DummyItem> getDummyItems() {
        return dummyItems;
    }

    public void setDummyItems(List<DummyContent.DummyItem> dummyItems) {
        this.dummyItems = dummyItems;
    }
}
