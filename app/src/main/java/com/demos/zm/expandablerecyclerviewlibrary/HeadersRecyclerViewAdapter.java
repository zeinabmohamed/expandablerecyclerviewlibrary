package com.demos.zm.expandablerecyclerviewlibrary;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demos.zm.expandablerecyclerviewlibrary.dummy.ParentItem;

import java.util.ArrayList;
import java.util.List;

public class HeadersRecyclerViewAdapter<T extends ChildRecyclerViewAdapter> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_HOLDER_HEADER_TYPE = 0;
    private static final int VIEW_HOLDER_CHILD_TYPE = 1;
    private final Context mContext;
    private final List<?> mBaseAdapterData;
    private final List mChildData;
    private boolean mValid = true;
    private int mSectionResourceId;
    private int mTextResourceId;
    private LayoutInflater mLayoutInflater;
    private T mBaseAdapter;
    private List mSections = new ArrayList();
    private List mTempSections = new ArrayList();
    private RecyclerView mRecyclerView;
    private boolean isExpandable = true;
    private int sectionsOffest = 0;

    public HeadersRecyclerViewAdapter(Context context, int sectionResourceId, int textResourceId, RecyclerView recyclerView,
                                      T baseAdapter, List<?> items) {

        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSectionResourceId = sectionResourceId;
        mTextResourceId = textResourceId;
        mBaseAdapter = baseAdapter;
        mContext = context;
        mRecyclerView = recyclerView;
        this.mBaseAdapterData = items;
        mChildData = new ArrayList<>();
        mChildData.addAll(items);


        mBaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyDataSetChanged();
            }

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeRemoved(positionStart, itemCount);
            }


        });

        // to handle grid span count case
        if (mRecyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager layoutManager = (GridLayoutManager) (mRecyclerView.getLayoutManager());
            layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return (isSectionHeaderPosition(position)) ? layoutManager.getSpanCount() : 1;
                }
            });
        }
    }

    /**
     * to toggle RecyclerView Headers be Expandable
     *
     * @param isExpandable
     */
    public void isExpandable(boolean isExpandable) {
        this.isExpandable = isExpandable;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int typeView) {
        if (typeView == VIEW_HOLDER_HEADER_TYPE) {
            final View view = LayoutInflater.from(mContext).inflate(mSectionResourceId, parent, false);
            return new HeaderViewHolder(view, mTextResourceId);
        } else {
            return mBaseAdapter.onCreateViewHolder(parent, typeView - 1);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder sectionViewHolder, final int position) {
        final Object item = mSections.get(position);
        if (mSections.get(position) instanceof Header) {
            ((HeaderViewHolder) sectionViewHolder).title.setText(((Header) item).title);
            if (isExpandable) {
                ((HeaderViewHolder) sectionViewHolder).title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (((Header) item).isExpanded) {
                            collapseParentListItem(position, ((Header) item).getChildsCount());
                        } else {
                            expandParentListItem(position, getParentIndex(item));
                        }
                    }
                });
            }
        } else {
            mBaseAdapter.onBindViewHolder(sectionViewHolder, getChildIndex(item));
        }
    }

    private int getParentIndex(Object item) {

        return mTempSections.indexOf(item);
    }

    private int getOrginalChildIndex(Object item) {

            int offeset = 0;
            int index = 0;
            for (int i = 0; i < mTempSections.size(); i++) {
                index = i;
                if (mTempSections.get(i) instanceof Header) {
                    offeset += 1;
                } else if (mTempSections.get(i).equals(item)) {
                    break;
                }
            }
            return index - offeset;

    }

    private int getChildIndex(Object item) {

        int offeset = 0;
        int index = 0;
        for (int i = 0; i < mSections.size(); i++) {
            index = i;
            if (mSections.get(i) instanceof Header) {
                offeset += 1;
            } else if (mSections.get(i).equals(item)) {
                break;
            }
        }
        return index - offeset;
    }

    @Override
    public int getItemViewType(int position) {

        if (mSections.get(position) instanceof Header) {
            return VIEW_HOLDER_HEADER_TYPE;
        } else {
            return VIEW_HOLDER_CHILD_TYPE;
        }
    }

    public void setData(List<ParentItem> items) {

        if (mSections != null) {
            mSections.clear();
        }
        int headerIndex = 0;
        for (int i = 0; i < items.size(); i++) {
            mSections.add(new Header(items.get(i).getTitle(), headerIndex, items.get(i).getDummyItems().size()));
            for (Object child : items.get(i).getDummyItems()) {
                mSections.add(child);
            }
            headerIndex += i + items.get(i).getDummyItems().size();
        }

        mTempSections.addAll(mSections);
        notifyDataSetChanged();
    }


    public boolean isSectionHeaderPosition(int position) {
        if (mSections.get(position) instanceof Header) {
            return true;
        } else {
            return false;
        }
    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
////        return isSectionHeaderPosition(position)
////                ? Integer.MAX_VALUE - mSections.indexOfKey(position)
////                : mBaseAdapter.getItemId(sectionedPositionToPosition(position));
//    }

    @Override
    public int getItemCount() {
        return mSections.size();
    }

    private void collapseParentListItem(int parentIndex, int childListItemCount) {


        for (int i = childListItemCount + parentIndex; i >= parentIndex + 1; i--) {
            mBaseAdapter.onCollapse(mSections.get(i));
            mSections.remove(i);
        }

        ((Header) mSections.get(parentIndex)).setExpanded(false);


        mBaseAdapter.notifyDataSetChanged();
        notifyItemRangeRemoved(parentIndex + 1, childListItemCount + parentIndex);

        notifyDataSetChanged();
    }

    private void expandParentListItem(int currentParentIndex,
                                      int orignalParentIndex) {

        if (mTempSections.get(orignalParentIndex) instanceof Header) {

            Header header = (Header) mTempSections.get(orignalParentIndex);
            int childsCounts = header.getChildsCount();

            ((Header) mSections.get(currentParentIndex)).setExpanded(true);
            for (int i = 0; i < childsCounts; i++) {


                mSections.add(currentParentIndex + i+1, mTempSections.get(i + orignalParentIndex+1));

                mBaseAdapter.onExpand(getOrginalChildIndex(mTempSections.get(i + orignalParentIndex+1))
                        ,mTempSections.get(i + orignalParentIndex+1));
            }



            mBaseAdapter.notifyDataSetChanged();
            notifyItemRangeInserted(currentParentIndex + 1, childsCounts);

            notifyDataSetChanged();


//        if (childItemList != null) {
//            int childListItemCount = childItemList.size();
//            for (int i = 0; i < childListItemCount; i++) {
//                mItemList.add(parentIndex + i + 1, childItemList.get(i));
//            }
//
//            notifyItemRangeInserted(parentIndex + 1, childListItemCount);
//        }
//
//        if (expansionTriggeredByListItemClick && mExpandCollapseListener != null) {
//            int expandedCountBeforePosition = getExpandedItemCount(parentIndex);
//            mExpandCollapseListener.onListItemExpanded(parentIndex - expandedCountBeforePosition);
//        }
//
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {

        public TextView title;

        public HeaderViewHolder(View view, int mTextResourceid) {
            super(view);
            title = (TextView) view.findViewById(mTextResourceid);
        }
    }

    public static class Header {

        int headerIndex;
        int childsCount;
        boolean isExpanded = true;
        String title;

        public Header(String title, int headerIndex, int childsCount) {

            this.title = title;
            this.childsCount = childsCount;
            this.headerIndex = headerIndex;
        }

        public int getHeaderIndex() {
            return headerIndex;
        }

        public void setHeaderIndex(int headerIndex) {
            this.headerIndex = headerIndex;
        }


        public String getTitle() {
            return title;
        }

        public int getChildsCount() {
            return childsCount;
        }

        public void setChildsCount(int childsCount) {
            this.childsCount = childsCount;
        }


        public boolean isExpanded() {
            return isExpanded;
        }

        public void setExpanded(boolean expanded) {
            isExpanded = expanded;
        }
    }


}