package com.demos.zm.expandablerecyclerviewlibrary;

import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by Zeinab Mohamed on 5/29/2016.
 */
public abstract class ChildRecyclerViewAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH>{

    public abstract void onCollapse(Object data);
    public abstract void onExpand(int startPosition, Object data);
    public abstract Object getFirstItem();

}
