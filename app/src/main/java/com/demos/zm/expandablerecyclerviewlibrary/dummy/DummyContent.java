package com.demos.zm.expandablerecyclerviewlibrary.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<ParentItem> ITEMS = new ArrayList<ParentItem>();


    public static final List<DummyItem> ITEMS_Child = new ArrayList<DummyItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
//    public static final Map<String, ParentItem> ITEM_MAP = new HashMap<String, ParentItem>();

    private static final int PARENT_COUNT = 5;
    private static final int COUNT = 5;

    static {
        // Add some sample items.
        int childIndex = 0;
        for (int i = 0; i < PARENT_COUNT; i++) {

            List<DummyItem> dummyItems = new ArrayList<>();

            for (int j = 0; j < COUNT; j++) {
                childIndex++;
                dummyItems.add(createDummyItem(childIndex));
            }
            ITEMS_Child.addAll(dummyItems);
            ITEMS.add(createParentItem(i,"Header "+i,dummyItems));
        }
    }

      private static DummyItem createDummyItem(int position) {
        return new DummyItem(String.valueOf(position), "Item " + position, makeDetails(position));
    }

    private static ParentItem createParentItem(int position, String title, List<DummyContent.DummyItem> dummyItems) {
        return new ParentItem(position, title, dummyItems);
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String id;
        public final String content;
        public final String details;

        public DummyItem(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }


}
