package com.demos.zm.expandablerecyclerviewlibrary;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demos.zm.expandablerecyclerviewlibrary.ItemFragment.OnListFragmentInteractionListener;
import com.demos.zm.expandablerecyclerviewlibrary.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyItemRecyclerViewAdapter extends ChildRecyclerViewAdapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final OnListFragmentInteractionListener mListener;
    private List<DummyItem> mValues;

    public MyItemRecyclerViewAdapter(List<DummyItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).content);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public List<DummyItem> getmValues() {
        return mValues;
    }

    public void setmValues(List<DummyItem> mValues) {
        clearmValues();
        this.mValues = mValues;
        notifyDataSetChanged();
    }

    public void clearmValues() {
        this.mValues.clear();
    }

    public void removeItemAtIndex(int index) {
        this.mValues.remove(index);
    }

    public void addItemAtIndex(int index, DummyItem dummyItem) {
        this.mValues.add(index, dummyItem);
    }


    @Override
    public void onCollapse(Object item) {

        mValues.remove(item);

    }

    @Override
    public void onExpand(int startPosition, Object data) {
        if(mValues.size()>startPosition) {
            mValues.add(startPosition, (DummyItem) data);
        }else {
            if(startPosition)
            mValues.add((DummyItem) data);
        }
    }

    @Override
    public Object getFirstItem() {
        return mValues.get(0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public DummyItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
